//
//  ProfileUIViewController.h
//  D4T
//
//  Created by rover on 2/6/14.
//  Copyright (c) 2014 Adaptive Dev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UseUIViewController.h"

@interface ProfileUIViewController : UseUIViewController
@property UIPageViewController *photoUIPageControl;
@property UIImageView *photoImageView;
@property UIView *onscreenUIView;
@property UIView *offscreenUIView;
@end
