//
//  ProfileUIViewController.m
//  D4T
//
//  Created by rover on 2/6/14.
//  Copyright (c) 2014 Adaptive Dev. All rights reserved.
//

#import "AppDelegate.h"
#import "ProfileUIViewController.h"
#import "ContentUIViewController.h"

UINavigationController *gUINavigationController;

@interface ProfileUIViewController ()

@end

@implementation ProfileUIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void) centerLayout:(NSMutableArray *)views parent:(UIView *)parent
                  NUM:(int)NUM_VIEWS CW:(int)CW CH:(int)CH CX_PAD:(int)CX_PAD CY_PAD:(int)CY_PAD CY_START:(int)CY_START {
    
    int OSI_MAX = (NUM_VIEWS - 1) / 2;  // offset index of button fom center.. e.g, 3 buttons, 1st is -1, 2nd is 0, 3rd is 1
    int OSI = -1*OSI_MAX - 1;
    for(int i = 0; i<NUM_VIEWS; i++)
    {
        UIView *view = [views objectAtIndex:i];
        view.frame = CGRectMake(self.SW/2 + OSI*CW + CW/2 + CX_PAD*(OSI+1), CY_START, CW, CH);
        [parent addSubview:views[i]];
        OSI++;
    }
}

- (void)dropRequested:(id)sender{
    NSLog(@"dropRequested");
    //[self handleSwipe:Nil dir:@"left" cxs:1 cxf:-1 oxs:3 oxf:1 cys:1 cyf:1 oys:1 oyf:1];
}

- (void)infoRequested:(id)sender{
    NSLog(@"infoRequested");
}

- (void)likeRequested:(id)sender{
    NSLog(@"likeRequested");
    //[gNavigationController pushViewController:gChatUIViewController animated:YES];
}

/*
- (void) displayContentController: (UIViewController*) content;
{
    [self addChildViewController:content];
    content.view.frame = CGRectMake(0,0,200,200);//self.view.frame;
    content.view.backgroundColor = [UIColor greenColor];
    [self.view addSubview:content.view];
    [content didMoveToParentViewController:self];
}
*/

- (void) viewWillAppear:(BOOL)animated
{
    NSLog(@"ProfileUIViewController: viewWillAppear");
    
    [super viewWillAppear:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSLog(@"ProfileUIViewController: viewDidLoad");
    
    
    self.view.backgroundColor = [UIColor yellowColor];
    
    _offscreenUIView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150,150)];
    //_offscreenUIView = [UIImage imageNamed:@"g0p0.jpg"];
    _offscreenUIView.backgroundColor = [UIColor blueColor];
    _offscreenUIView.userInteractionEnabled = YES;
    _offscreenUIView.contentMode  = UIViewContentModeScaleAspectFit;
    
    _offscreenUIView = [[UIView alloc] initWithFrame:CGRectMake(30, 30, 150,150)];
    //offUIImageView.image = [UIImage imageNamed:@"g1p0.jpg"];
    _offscreenUIView.backgroundColor = [UIColor redColor];
    _offscreenUIView.userInteractionEnabled = YES;
    _offscreenUIView.contentMode  = UIViewContentModeScaleAspectFit;
    
    SwipeUIViewController *contentVC = [[SwipeUIViewController alloc] init];
    contentVC.view = [[UIView alloc] initWithFrame:CGRectMake(0,0,250, 250)];
    contentVC.view.backgroundColor = [UIColor magentaColor];
    [self.view addSubview:contentVC.view];
    [contentVC didMoveToParentViewController:self];
    
    //[contentVC.view addSubview:onUIImageView];
    
    [contentVC mySetViews:_onscreenUIView offscreen:_offscreenUIView];
    
    [contentVC myAddViews];
    
    //int _xOrigin = 0;
    int _yOrigin = 5*self.CP;
    
    UIView *bottomUIView = [[UIView alloc] initWithFrame:CGRectMake(0, _yOrigin + self.SW, self.SW, self.SH - self.SW - _yOrigin)];
    bottomUIView.userInteractionEnabled=YES;
    bottomUIView.backgroundColor = [UIColor yellowColor];
    [self.view addSubview:bottomUIView];
    
    // INFO LABELS
    
    int bh = bottomUIView.frame.size.height;
    int tp = self.CP/4; // text padding
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(tp, -self.CP/3, self.SW-2*tp, bh/4)];
    UIScrollView *aboutScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(tp, 0.75*bh/4, self.SW-2*tp, bh/3)];
    aboutScroll.backgroundColor = [UIColor cyanColor];
    aboutScroll.contentSize = CGSizeMake(self.SW-2*tp, self.CP*4);
    UILabel *aboutLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.SW-2*tp, self.CP*4)];
    [aboutScroll addSubview:aboutLabel];
    //UIScrollView *aboutText = [[UITextView alloc] initWithFrame:CGRectMake(tp, 0.75*bh/4, self.SW-2*tp, bh/3)];
    //aboutText.editable=NO;
    //UILabel *friendsLabel = [[UILabel alloc] initWithFrame:CGRectMake(tp, 2.5*self.CM, self.SW-2*tp, self.CM)];
    UILabel *interestsLabel = [[UILabel alloc] initWithFrame:CGRectMake(tp, 2.0*bh/4, self.SW-2*tp, bh/4)];
    
    [nameLabel setFont:[UIFont fontWithName:@"Arial" size:12]];
    [aboutLabel setFont:[UIFont fontWithName:@"Arial" size:12]];
    //[friendsLabel setFont:[UIFont fontWithName:@"Arial" size:12]];
    [interestsLabel setFont:[UIFont fontWithName:@"Arial" size:12]];
    
    nameLabel.text = @"Jane 25; text not fitting; 2 km away; seen 11 min ago";
    nameLabel.numberOfLines = 1;
    nameLabel.adjustsFontSizeToFitWidth = YES;
    aboutLabel.text = @"About Me: Hi, I'm Jane and this is my multi-line description. The options for laying it out are scrolling left or right or expanding.";
    aboutLabel.numberOfLines = 4;
    //friendsLabel.text = @"Friends <Icon>: <picture> <picture>";
    interestsLabel.text = @"<Interests>: <picture> <picture>";
    
    int num_buttons = 3;
    int osi = (num_buttons - 1) / 2;  // offset index of button fom center.. e.g, 3 buttons, 1st is -1, 2nd is 0, 3rd is 1
    osi *= -1; // 1st button
    int cw = 2*self.CM; // control width
    int ch = bh/4; //self.CM; // control height
    int cx_pad = self.CP; // X padding space
    int cy_pad = 0;//self.CP;
    int cy_start = self.SH-bottomUIView.frame.origin.y-ch-cy_pad; // starting position
    
    // OPTIONS
    UIView *optionsUIView = [[UIView alloc] init];
    optionsUIView.backgroundColor = [UIColor cyanColor];
    
    // BOTTOM BUTTONS - "Drop", "info", "Date"
    UIButton *dropButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    UIButton *likeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [dropButton setTitle:@"X"       forState:UIControlStateNormal];
    [infoButton setTitle:@"info"    forState:UIControlStateNormal];
    [likeButton setTitle:@"Like"   forState:UIControlStateNormal];
    dropButton.backgroundColor = [UIColor brownColor];
    infoButton.backgroundColor = [UIColor blueColor];
    likeButton.backgroundColor = [UIColor redColor];
    [dropButton addTarget:self action:@selector(dropRequested:) forControlEvents:UIControlEventTouchDown];
    [infoButton addTarget:self action:@selector(infoRequested:) forControlEvents:UIControlEventTouchDown];
    [likeButton addTarget:self action:@selector(likeRequested:) forControlEvents:UIControlEventTouchDown];
    NSMutableArray *viewArray = [NSMutableArray array];
    [viewArray addObject:dropButton];
    [viewArray addObject:infoButton];
    [viewArray addObject:likeButton];
    [self centerLayout:viewArray parent:bottomUIView NUM:num_buttons CW:cw CH:ch CX_PAD:cx_pad CY_PAD:cy_pad CY_START:cy_start];
    [optionsUIView addSubview:dropButton];
    [optionsUIView addSubview:infoButton];
    [optionsUIView addSubview:likeButton];
    
    [bottomUIView addSubview:nameLabel];
    [bottomUIView addSubview:aboutScroll];
    //[bottomUIView addSubview:friendsLabel];
    [bottomUIView addSubview:interestsLabel];
    
    [bottomUIView addSubview:optionsUIView];
    
    _photoUIPageControl = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl
                                                          navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    //[_photoUIPageControl spineLocation:8];
    
    /*
     int size = 200;
     _photoUIPageControl.view.frame = CGRectMake(0, 0, size, size);
     _photoUIPageControl.view.backgroundColor = [UIColor greenColor];
     [self addChildViewController:_photoUIPageControl];
     
     const int NUM_PHOTOS_MAX = 1;
     NSMutableArray *array = [[NSMutableArray alloc] init];
     for(int i=0; i<NUM_PHOTOS_MAX; i++)
     {
     UIViewController *vc = [[UIViewController alloc] init];
     vc.view.frame = CGRectMake(0, 0, size, size);
     vc.view.backgroundColor = [UIColor brownColor];
     
     UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, size, size)];
     imgView.backgroundColor = [UIColor blueColor];
     imgView.image = [UIImage imageNamed:@"g0p0.jpg"];
     imgView.contentMode = UIViewContentModeCenter;
     [vc.view addSubview:imgView];
     [array addObject:vc];
     }
     [_photoUIPageControl setViewControllers:array direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
     
     */
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
