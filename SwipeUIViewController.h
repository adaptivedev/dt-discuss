//
//  SwipeUIViewController.h
//  D4T
//
//  Created by rover on 1/24/14.
//  Copyright (c) 2014 Adaptive Dev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileUIViewController.h"

@interface SwipeUIViewController : UIViewController
// For testing, won't use array, will get 1-by-1 from server
@property NSMutableArray *viewArray;
@property int viewArrayCursor;
@property int xOrigin;
@property int yOrigin;
@property float swipeSpeed;
@property UIView *currentUIView;
@property UIView *offscreenUIView;
@property ProfileUIViewController* currentProfileUIViewController;
@property ProfileUIViewController* offscreenProfileUIViewController;
- (UIImageView *)mySetView:(UIView *)name;
- (void)mySetViews:(UIView *)onscreen offscreen:(UIView *)offscreen;
- (void)myAddViews;
@property int SW;
@property int CP;
@end
