//
//  SwipeUIViewController.m
//  D4T
//
//  Created by rover on 1/24/14.
//  Copyright (c) 2014 Adaptive Dev. All rights reserved.
//

#import "AppDelegate.h"
#import "SwipeUIViewController.h"
#import "ProfileUIViewController.h"

ChatUIViewController *gChatUIViewController;

@interface SwipeUIViewController ()

@end

@implementation SwipeUIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    _xOrigin = 0;
    _yOrigin = 5*self.CP;
    _swipeSpeed = 0.3;
    
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    NSLog(@"SwipeUIViewController: viewWillAppear");
 
    [super viewWillAppear:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    NSLog(@"SwipeUIViewController: viewDidLoad");
}

- (void)mySetViews:(UIView *)onscreen offscreen:(UIView *)offscreen
{
    _currentUIView = onscreen;
    _offscreenUIView = offscreen;
    
    [self mySetView:onscreen];
    [self mySetView:offscreen];
}

- (void)myAddViews
{
    [self.view addSubview:_currentUIView];
    [self.view addSubview:_offscreenUIView];
}


- (float) distanceSquared:(CGPoint) p1 p2:(CGPoint) p2 {
	float dx = (p1.x-p2.x);
	float dy = (p1.y-p2.y);
	return dx*dx+dy*dy;
}

void sort(int*a,int n,int*ndx) {
	int*b,*c,t;
	int*bi,*ci,ti;
	for(b=a+n,bi=ndx+n ; --b>a;) {
		--bi;
		for(c=b,ci=bi;--c>=a;) {
			--ci;
			if(*c>*b)t=*b,*b=*c,*c=t,ti=*bi,*bi=*ci,*ci=ti;
		}
	}
}

- (void)mySwapImages
{
    NSLog(@"mySwapImages");
    UIView *tmp = _currentUIView;
    _currentUIView = _offscreenUIView;
    _offscreenUIView = tmp;
    
    //[self setImages];
}

- (UIView *)mySetView:(UIView *)theView
{
    NSLog(@"mySetView");
    
    theView.userInteractionEnabled = YES;
    theView.contentMode  = UIViewContentModeScaleAspectFit;
    
    [self setUISwipeGestureRecognizer:UISwipeGestureRecognizerDirectionRight view:theView sel:@selector(handleSwipeRight:)];
    [self setUISwipeGestureRecognizer:UISwipeGestureRecognizerDirectionLeft view:theView sel:@selector(handleSwipeLeft:)];
    [self setUISwipeGestureRecognizer:UISwipeGestureRecognizerDirectionUp view:theView sel:@selector(handleSwipeUp:)];
    [self setUISwipeGestureRecognizer:UISwipeGestureRecognizerDirectionDown view:theView sel:@selector(handleSwipeDown:)];
    
    //UIPinchGestureRecognizer *pgr = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    //pgr.delegate = self;
    //[imgView addGestureRecognizer:pgr];
    //UIPanGestureRecognizer *panRecog = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    //[imgView addGestureRecognizer:panRecog];
    //panRecog.delegate = self;
    
    return theView;
}

- (void)setUISwipeGestureRecognizer:(UISwipeGestureRecognizerDirection)dir view:(UIView *)view sel:(SEL)sel {
    //NSLog(@"makeUISwipeGestureRecognizer for view=%@", view);
    UISwipeGestureRecognizer *rec = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:sel];
    [rec setDirection:(dir)];
    [view addGestureRecognizer:rec];
}

- (IBAction)handleSwipe:(UIPanGestureRecognizer *)recognizer dir:(NSString *)dir
                    cxs:(int)cxs cxf:(int)cxf oxs:(int)oxs oxf:(int)oxf cys:(int)cys cyf:(int)cyf oys:(int)oys oyf:(int)oyf
{
    NSLog(@"handleSwipe %@: %@", dir, recognizer);
    _offscreenUIView.hidden=NO;
    _currentUIView.hidden=NO;
    if([dir  isEqual: @"left"] || [dir  isEqual: @"down"])
        [self serverProvideImageNext];
    else
        [self serverProvideImagePrev];
    // Put image in left
    CGPoint startPointCurr = CGPointMake(_xOrigin + cxs*self.SW/2, _yOrigin + cys*self.SW/2);
    _currentUIView.center = startPointCurr;
    [UIView animateWithDuration: _swipeSpeed
                          delay: 0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGPoint finalPoint = CGPointMake(_xOrigin + cxf*self.SW/2, _yOrigin + cyf*self.SW/2);
                         _currentUIView.center = finalPoint; }
                     completion:^(BOOL finished){
                         NSLog(@"handleSwipe %@ completion current", dir);
                     }];
    // Put right image in center
    CGPoint startPointOff = CGPointMake(_xOrigin + (oxs*self.SW/2), _yOrigin + oys*self.SW/2);
    _offscreenUIView.center = startPointOff;
    [UIView animateWithDuration: _swipeSpeed
                          delay: 0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         //_offscreenUIView.backgroundColor = [UIColor redColor];
                         CGPoint finalPoint = CGPointMake(_xOrigin + oxf*self.SW/2, _yOrigin + oyf*self.SW/2);
                         _offscreenUIView.center = finalPoint; }
                     completion:^(BOOL finished){
                         NSLog(@"handleSwipe %@ completion offscreen", dir);
                         [self mySwapImages];
                         _offscreenUIView.hidden=YES;
                     }];
}

- (IBAction)handleSwipeLeft:(UIPanGestureRecognizer *)recognizer {
    NSLog(@"handleSwipeLeft: %@", recognizer);
    //return [self handleSwipe:recognizer dir:@"left" cxs:1 cxf:-1 oxs:3 oxf:1 cys:1 cyf:1 oys:1 oyf:1];
}

- (IBAction)handleSwipeRight:(UIPanGestureRecognizer *)recognizer {
    NSLog(@"handleSwipeRight: %@", recognizer);
    //return [self handleSwipe:recognizer dir:@"right" cxs:1 cxf:3 oxs:-1 oxf:1 cys:1 cyf:1 oys:1 oyf:1];
}

- (IBAction)handleSwipeUp:(UIPanGestureRecognizer *)recognizer {
    NSLog(@"handleSwipeUp: %@", recognizer);
    return [self handleSwipe:recognizer dir:@"up" cxs:1 cxf:1 oxs:1 oxf:1 cys:1 cyf:-1 oys:3 oyf:1];
}

- (IBAction)handleSwipeDown:(UIPanGestureRecognizer *)recognizer {
    NSLog(@"handleSwipeDown: %@", recognizer);
    return [self handleSwipe:recognizer dir:@"down" cxs:1 cxf:1 oxs:1 oxf:1 cys:1 cyf:3 oys:-1 oyf:1];
}

- (void)doneSwipeRight {
    NSLog(@"doneSwipeRight");
}

- (void)serverProvideImageNext
{
    _viewArrayCursor++;
    NSLog(@"serverProvideImagePrev: _viewArrayCursor=%i", _viewArrayCursor);
    if(_viewArrayCursor >= [_viewArray count]) _viewArrayCursor = 0;
    NSLog(@"serverProvideImageNext: 2: _viewArrayCursor=%i", _viewArrayCursor);
    NSString *imgName = _viewArray[_viewArrayCursor];
    _offscreenUIView = (UIView *)[UIImage imageNamed:imgName];
}

- (void)serverProvideImagePrev
{
    _viewArrayCursor--;
    NSLog(@"serverProvideImagePrev: _viewArrayCursor=%i", _viewArrayCursor);
    if(_viewArrayCursor < 0) _viewArrayCursor = [_viewArray count] - 1.0;
    NSLog(@"serverProvideImagePrev: 2: _viewArrayCursor=%i", _viewArrayCursor);
    NSString *imgName = _viewArray[_viewArrayCursor];
    _offscreenUIView = (UIView *)[UIImage imageNamed:imgName];
}

/*
 - (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer {
 
 //NSLog(@"handlePan: recognizer.view.center.x=%f recognizer.view.center.y=%f, %@", recognizer.view.center.x, recognizer.view.center.y, recognizer);
 
 CGPoint translation = [recognizer translationInView:_imgView1];
 
 recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x, _yOrigin + recognizer.view.frame.size.height/2);
 _imgView0.center = CGPointMake(_imgView0.center.x + translation.x, _yOrigin + _imgView0.frame.size.height/2);
 _imgView2.center = CGPointMake(_imgView2.center.x + translation.x, _yOrigin + _imgView2.frame.size.height/2);
 
 
 if (recognizer.state == UIGestureRecognizerStateEnded) {
 
 // Check here for the position of the view when the user stops touching the screen
 
 // Set "CGFloat finalX" and "CGFloat finalY", depending on the last position of the touch
 
 // Put prev image back to prev screen
 [UIView animateWithDuration: 0.2
 delay: 0
 options: UIViewAnimationOptionCurveEaseOut
 animations:^{
 CGPoint finalPoint = CGPointMake(_xOrigin - 1*_gScreenW/2, _yOrigin + _gScreenW/2);
 _imgView0.center = finalPoint; }
 completion:nil];
 
 // Put image back to center
 [UIView animateWithDuration: 0.2
 delay: 0
 options: UIViewAnimationOptionCurveEaseOut
 animations:^{
 CGPoint finalPoint = CGPointMake(_xOrigin + _gScreenW/2, _yOrigin + _gScreenW/2);
 recognizer.view.center = finalPoint; }
 completion:nil];
 
 // Put next image back to next screen
 [UIView animateWithDuration: 0.2
 delay: 0
 options: UIViewAnimationOptionCurveEaseOut
 animations:^{
 CGPoint finalPoint = CGPointMake(_xOrigin + 3*_gScreenW/2, _yOrigin + _gScreenW/2);
 _imgView2.center = finalPoint; }
 completion:nil];
 
 }
 
 [recognizer setTranslation:CGPointMake(0, 0) inView:_imgView1];
 }
 */

- (void)handlePinch:(UIPinchGestureRecognizer *)pinchGestureRecognizer
{
    //handle pinch...
    NSLog(@"handlePinch: %@", pinchGestureRecognizer);
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    NSLog(@"MOVE gestureRecognizer: %@", gestureRecognizer);
    return true;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
